import { FaturamentoTipoEstabelecimento } from './models/faturamento-tipo.-estabelecimentomodel';
import { tap, map } from 'rxjs/operators';
import { Mes } from './../shared/models/mes.model';
import { MesService } from './../shared/services/mes.service';
import { SidebarService } from './../shared/services/sidebar.service';
import { Engajamento } from './models/engajamento.model';
import { HttpResponse } from '@angular/common/http';
import { DashboardService } from './dashboard.service';
import { Faturamento } from './models/faturamento.model';
import { Component, OnInit, OnDestroy } from '@angular/core';
import * as Highcharts from 'highcharts';
import * as HighchartsExporting from 'highcharts/modules/exporting';
import { Unsubscribable, Observable } from 'rxjs';
import { MatSelectChange } from '@angular/material/select';
// @ts-ignore
HighchartsExporting(Highcharts);


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy {

  public Highcharts: typeof Highcharts = Highcharts;
  public faturamentos: Faturamento[];

  public meses$: Observable<Mes[]>;

  //Atributos privados
  private _unsubscribeEventoSidebar: Unsubscribable;

  //Configuração do grafico historico de vendas
  public chartEngajamentosOptions: Highcharts.Options;
  public engajamentos: Engajamento[];
  private _superMercados: number[];
  private _restaurantes: number[];
  private _variedades: number[];
  private _moda: number[];
  private _meses: string[];

  //Configuração do grafico faturamento.
  public chartFaturamentoMensalOptions: Highcharts.Options;
  public faturamento: Faturamento;
  public faturamentoPorEstabelecimento: FaturamentoTipoEstabelecimento;

  constructor(
    private _dashBoardService: DashboardService,
    private _sidebarService: SidebarService,
    private _mesService: MesService
  ) { }


  ngOnInit(): void {
    this.obterMeses();
    this._obterEventoSidebar();
    this._obterFaturamentos();
    this._obterEngajamentos();
  }

  ngOnDestroy(): void {
    this._unsubscribeEventoSidebar.unsubscribe();
  }

  public obterMeses(): void{
    this.meses$ = this._mesService.obterMeses().pipe(map(r => r.body.sort((a,b) => a.id < b.id ? 1 : -1)));
  }

  public trocarMes(select: MatSelectChange): void{
    if(select){
      this._obterFaturamentoMensalParaGrafico(select.value);
    }

  }

  private _obterFaturamentoPorTipo(fatSupermercado: number[], fatRestaurante: number[], fatVariedade: number[], fatModa: number[]): void{
    const supermercado = fatSupermercado.reduce((total, valor) => total + valor) / fatSupermercado.length;
    const restaurante = fatRestaurante.reduce((total, valor) => total + valor) / fatRestaurante.length;
    const variedade = fatVariedade.reduce((total, valor) => total + valor) / fatVariedade.length;
    const moda = fatModa.reduce((total, valor) => total + valor) / fatModa.length;
    this.faturamentoPorEstabelecimento = {supermercado, restaurante, variedade, moda};
  }

  private _obterEngajamentos(): void{
    this._dashBoardService.obterEngajamento().subscribe(
      (res: HttpResponse<Engajamento[]>) => {
        if(res.status == 200){
          if(res.body && res.body.length){
            this.engajamentos = res.body;
            this._superMercados = res.body.map(f => f.superMercado);
            this._restaurantes = res.body.map(f => f.restaurante);
            this._variedades = res.body.map(f => f.variedades);
            this._moda = res.body.map(f => f.moda);
            this._meses = res.body.map(f => f.meses.nome);

            this._obterFaturamentoPorTipo(this._superMercados, this._restaurantes, this._variedades, this._moda);
            this.chartEngajamentosOptions = this._criarGraficoEngajamento();
          }
        }
      });
  }

  private _obterFaturamentoMensalParaGrafico(mesId: number): void{
    if(mesId){
      this.faturamento = this.faturamentos.find(r => r.meses.id == mesId);
      this.chartFaturamentoMensalOptions = this._criarGraficoFaturamentoMensal();
    }
  }

  private _obterFaturamentos(): void{
    this._dashBoardService.obterFaturamento().subscribe(
      (res: HttpResponse<Faturamento[]>) => {
        if(res.status == 200){
          if(res.body && res.body.length){
            this.faturamentos = res.body.sort((a, b) => a.meses.id < b.meses.id ? 1 : -1);

            if(this.faturamentos?.length){
              this._obterFaturamentoMensalParaGrafico(this.faturamentos[0]?.meses?.id);
            }
          }
        }
      });
  }

  /**
 * Cria o grafico de historico de vendas.
 * @param superMercado
 * @param restaurante
 * @param variedades
 * @param vendaEnergetico
 * @param moda
 */
private _criarGraficoEngajamento(): Highcharts.Options{
  let chartEngajamentosOptions: Highcharts.Options = {
    chart: {
      type: 'line'
  },
  credits: {
    enabled: false
  },
    title: {
      text: 'Engajamento por mês'
  },
  yAxis: {
      title: {
          text: 'Faturamento'
      }
  },
  xAxis: {
    categories: this._meses
  },
  plotOptions: {
    line: {
      dataLabels: {
          enabled: true
      },
      enableMouseTracking: false
  }
  },
  exporting:{
    enabled:true,
    buttons: {
      contextButton: {
        // @ts-ignore
            menuItems: [{
          textKey: 'viewFullscreen',
          text: 'Visualizar em Tela Cheia',
        },// @ts-ignore
         {
          separator: true
        },
        // @ts-ignore
         {
          textKey: 'downloadPNG',
          text: 'Download em PNG',
          onclick: function () {
            this.exportChart();
          }
        },// @ts-ignore
         {
          textKey: 'downloadJPEG',
          text: 'Download em JPEG',
          onclick: function () {
            this.exportChart({
              type: 'image/jpeg'
            });
          }
        }, // @ts-ignore
        {
          textKey: 'downloadPDF',
          text: 'Download em PDF',
          onclick: function () {
            this.exportChart({
              type: 'application/pdf'
            });
          }
        }]
      }
    }
  },
  series: [{
      name: 'Supermercado',
      // @ts-ignore
      data: this._superMercados,
      color: '#dc3545'
  }, {
      name: 'Restaurante',
      // @ts-ignore
      data: this._restaurantes,
      color: '#01579B'
  }, {
      name: 'Variedades',
      // @ts-ignore
      data: this._variedades,
      color: '#4CAF50'
  },
  {
    name: 'Moda',
    // @ts-ignore
    data: this._moda,
    color: '#FFC400'
}],

  responsive: {
      rules: [{
          condition: {
              maxWidth: 500
          },
          chartOptions: {
              legend: {
                  layout: 'horizontal',
                  align: 'center',
                  verticalAlign: 'bottom'
              }
          }
      }]
  }
  }
  return chartEngajamentosOptions;
}

/**
 * Cria o grafico de faturamento mensal.
 * @param faturamento
 */
private _criarGraficoFaturamentoMensal(): Highcharts.Options{
  let _chartFaturamentoMensalOptions: Highcharts.Options = {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
  },
  credits: {
    enabled: false
  },
  title: {
      text: `Mês: ${this.faturamento.meses.nome}`
  },
  tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
  },
  accessibility: {
      point: {
          valueSuffix: '%'
      }
  },
  plotOptions: {
      pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
              enabled: true,
              format: '<b>{point.name}</b>: {point.percentage:.1f} %'
          }
      }
  },
  exporting:{
    enabled:true,
    buttons: {
      contextButton: {
        // @ts-ignore
            menuItems: [{
          textKey: 'viewFullscreen',
          text: 'Visualizar em Tela Cheia',
        },// @ts-ignore
         {
          separator: true
        },
        // @ts-ignore
         {
          textKey: 'downloadPNG',
          text: 'Download em PNG',
          onclick: function () {
            this.exportChart();
          }
        },// @ts-ignore
         {
          textKey: 'downloadJPEG',
          text: 'Download em JPEG',
          onclick: function () {
            this.exportChart({
              type: 'image/jpeg'
            });
          }
        }, // @ts-ignore
        {
          textKey: 'downloadPDF',
          text: 'Download em PDF',
          onclick: function () {
            this.exportChart({
              type: 'application/pdf'
            });
          }
        }]
      }
    }
  },
  // @ts-ignore
  series: [{
      name: 'Vendas',
      colorByPoint: true,
      data: [{
          name: 'Supermercado',
          y: this.faturamento.superMercado,
          color: '#dc3545'
      }, {
          name: 'Restaurante',
          y: this.faturamento.restaurante,
          color: '#01579B'
      }, {
          name: 'Variedades',
          y: this.faturamento.variedades,
          color: '#4CAF50'
      },{
        name: 'Moda',
        y: this.faturamento.moda,
        color: '#FFC400'
    }]
  }]
}
return _chartFaturamentoMensalOptions;
}


private _obterEventoSidebar(): void{
  this._unsubscribeEventoSidebar = this._sidebarService.obterEvento().subscribe(
    res => {
      this.chartEngajamentosOptions = null;
      this.chartFaturamentoMensalOptions = null;
      setTimeout(() => {
        this.chartEngajamentosOptions = this._criarGraficoEngajamento();
        this.chartFaturamentoMensalOptions = this._criarGraficoFaturamentoMensal();
      }, 300);
    }
  )
};

}
