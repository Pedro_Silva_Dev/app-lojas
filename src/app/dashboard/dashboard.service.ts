import { Engajamento } from './models/engajamento.model';
import { Faturamento } from './models/faturamento.model';
import { Historico } from './models/historico.model';
import { take } from 'rxjs/operators';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(
    private _http: HttpClient
  ) { }

  public obterHistorico(mesId: number): Observable<HttpResponse<Historico>>{
    return this._http.get<Historico>(`${environment.BASE_URL}/historico?_expand=ultimosMeses&ultimosMesesId=${mesId}`, {observe: 'response'}).pipe(take(1));
  }

  public obterEngajamento(): Observable<HttpResponse<Engajamento[]>>{
    return this._http.get<Engajamento[]>(`${environment.BASE_URL}/engajamentos?_expand=meses`, {observe: "response"}).pipe(take(1));
  }

  public obterFaturamento(): Observable<HttpResponse<Faturamento[]>>{
    return this._http.get<Faturamento[]>(`${environment.BASE_URL}/faturamentos?_expand=meses`, {observe: "response"}).pipe(take(1));
  }

}
