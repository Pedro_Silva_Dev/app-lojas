import { Mes } from "../../shared/models/mes.model";

export interface Engajamento{
  id: number;
  superMercado: number;
  restaurante: number;
  moda: number;
  variedades: number;
  meses: Mes;
}
