export interface Historico{
  id: number;
  avaliacao: string;
  aderencia: string;
  alcance: string;
  produtividade: string;
  ultimosMesesId: number;
}
