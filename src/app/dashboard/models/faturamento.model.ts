import { Mes } from 'src/app/shared/models/mes.model';

export interface Faturamento{
  id: number;
  superMercado: number;
  restaurante: number;
  moda: number;
  variedades: number;
  meses: Mes;
}
