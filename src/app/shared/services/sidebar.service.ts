import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {

  private _reloadEvent: EventEmitter<void> = new EventEmitter<void>();

  constructor() { }

  public obterEvento(): EventEmitter<void>{
    return this._reloadEvent;
  }

  public dispararEvento(): void{
    this._reloadEvent.emit();
  }

}
