import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Mes } from '../models/mes.model';

@Injectable({
  providedIn: 'root'
})
export class MesService {

  constructor(private _http: HttpClient) { }

  public obterMeses(): Observable<HttpResponse<Mes[]>>{
    return this._http.get<Mes[]>(`${environment.BASE_URL}/meses`, {observe: "response"}).pipe(take(1));
  }


}
