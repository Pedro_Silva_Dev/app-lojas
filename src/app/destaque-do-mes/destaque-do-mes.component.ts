import { MelhorEstabelecimento } from './models/melhor-estabelecimento.model';
import { HttpResponse } from '@angular/common/http';
import { DestaqueDoMesService } from './destaque-do-mes.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-destaque-do-mes',
  templateUrl: './destaque-do-mes.component.html',
  styleUrls: ['./destaque-do-mes.component.css']
})
export class DestaqueDoMesComponent implements OnInit {

  private _badgeEstabelecimento = [
    {nome: 'supermercado', badge: 'badge badge-red'},
    {nome: 'restaurante', badge: 'badge badge-blue'},
    {nome: 'variedade', badge: 'badge badge-green'},
    {nome: 'moda', badge: 'badge badge-yellow'},
  ]

  public melhoresEstabelecimentos: MelhorEstabelecimento[];
  public pagina: number = 0;
  public tamanhoPadrao: number = 6;
  public totalDeElementos: number;

  constructor(
    private _destaqueMesService: DestaqueDoMesService
  ) { }

  ngOnInit(): void {
    this.exibirMelhores(this.pagina);
  }

  public exibirMelhores(pagina: number): void{
    this._destaqueMesService.obterMelhoresDoMes(pagina, this.tamanhoPadrao).subscribe(
      (res: HttpResponse<MelhorEstabelecimento[]>) => {
        if(res.status == 200){
          this.melhoresEstabelecimentos = res.body;
          this.totalDeElementos = Number.parseInt(res.headers.get('X-Total-Count'));
        }
      });
  }

  public obterBadge(estabelecimento: string): string{
    let badge = this._badgeEstabelecimento.find(e => e.nome.toLocaleUpperCase() === estabelecimento.toLocaleUpperCase());
    return badge ? badge.badge : '';
  }

}
