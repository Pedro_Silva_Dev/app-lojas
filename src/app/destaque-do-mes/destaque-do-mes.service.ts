import { take } from 'rxjs/operators';
import { environment } from './../../environments/environment';
import { MelhorEstabelecimento } from './models/melhor-estabelecimento.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DestaqueDoMesService {

  constructor(
    private _http: HttpClient
  ) { }

  public obterMelhoresDoMes(pagina: number, tamanho: number): Observable<HttpResponse<MelhorEstabelecimento[]>>{
    return this._http.get<MelhorEstabelecimento[]>(`${environment.BASE_URL}/melhoresEstabelecimentos?_page=${pagina}&_limit=${tamanho}`, {observe: "response"}).pipe(take(1));
  }

}
