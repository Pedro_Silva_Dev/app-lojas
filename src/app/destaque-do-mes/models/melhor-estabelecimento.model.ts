export interface MelhorEstabelecimento{
  id: number;
  nome: string;
  mesAno: string;
  avaliacao: string;
  aderencia: string;
  tipo: string;
}
