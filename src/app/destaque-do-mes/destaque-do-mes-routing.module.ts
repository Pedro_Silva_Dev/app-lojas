import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DestaqueDoMesComponent } from './destaque-do-mes.component';

const routes: Routes = [{ path: '', component: DestaqueDoMesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DestaqueDoMesRoutingModule { }
