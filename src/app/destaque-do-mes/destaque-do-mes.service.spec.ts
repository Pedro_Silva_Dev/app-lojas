import { TestBed } from '@angular/core/testing';

import { DestaqueDoMesService } from './destaque-do-mes.service';

describe('DestaqueDoMesService', () => {
  let service: DestaqueDoMesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DestaqueDoMesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
