import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DestaqueDoMesRoutingModule } from './destaque-do-mes-routing.module';
import { DestaqueDoMesComponent } from './destaque-do-mes.component';


@NgModule({
  declarations: [DestaqueDoMesComponent],
  imports: [
    CommonModule,
    DestaqueDoMesRoutingModule,
    SharedModule
  ]
})
export class DestaqueDoMesModule { }
