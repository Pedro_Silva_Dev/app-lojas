import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DestaqueDoMesComponent } from './destaque-do-mes.component';

describe('DestaqueDoMesComponent', () => {
  let component: DestaqueDoMesComponent;
  let fixture: ComponentFixture<DestaqueDoMesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DestaqueDoMesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DestaqueDoMesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
