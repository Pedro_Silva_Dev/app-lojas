import { SidebarService } from './shared/services/sidebar.service';
import { AuthService } from './auth/auth.service';
import { Component } from '@angular/core';
import { User } from './auth/models/user.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app-lojas';

  constructor(
    private _authService: AuthService,
    private _sidebarService: SidebarService
  ){}

  public obterUsuario(): User{
    return this._authService.obterUsuarioLogado();
  }

  public sidebar(drawer: any): void{
    this._sidebarService.dispararEvento();
    drawer.toggle();
  }

}
