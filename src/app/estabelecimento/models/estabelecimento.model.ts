export interface Estabelecimento {
  id: number;
  nome: string;
  qtdeVendas: number;
  avaliacao: number;
  aderencia: number;
  tipo: string;
  status: string;
};
