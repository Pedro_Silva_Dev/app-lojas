import { map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { EstabelecimentoService } from './estabelecimento.service';
import { Component, OnInit } from '@angular/core';
import { Estabelecimento } from './models/estabelecimento.model';

@Component({
  selector: 'app-estabelecimento',
  templateUrl: './estabelecimento.component.html',
  styleUrls: ['./estabelecimento.component.css']
})
export class EstabelecimentoComponent implements OnInit {

  public pagina:number = 1;
  public tamanho: number = 12;
  public totalDeElementos: number;

  public estabelecimentos$: Observable<Estabelecimento[]>;

  constructor(
    private _estabelecimentoService: EstabelecimentoService
  ) { }

  ngOnInit(): void {
    this.obterEstabelecimentos(this.pagina);
  }

  public obterEstabelecimentos(pagina: number): void{
    this.estabelecimentos$ = this._estabelecimentoService.obterPaginaEstabelecimento(pagina, this.tamanho).pipe(
      map(e => {
        this.totalDeElementos = Number.parseInt(e.headers.get('X-Total-Count'));
        return e.body.sort((a, b) => a.id < b.id ? 1 : -1)
    }));
  }

}
