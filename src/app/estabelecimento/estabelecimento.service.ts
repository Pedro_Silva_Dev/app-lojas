import { take } from 'rxjs/operators';
import { environment } from './../../environments/environment';
import { Estabelecimento } from './models/estabelecimento.model';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EstabelecimentoService {

  constructor(
    private _http: HttpClient
  ) { }

  public obterPaginaEstabelecimento(pagina: number, tamanho: number): Observable<HttpResponse<Estabelecimento[]>>{
    return this._http.get<Estabelecimento[]>(`${environment.BASE_URL}/estabelecimentos?_page=${pagina}&_limit=${tamanho}`, {observe: "response"}).pipe(take(1));
  }

}
