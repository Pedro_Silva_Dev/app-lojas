import { ValidationsService } from './../../shared/validations/validations.service';
import { AuthService } from './../auth.service';
import { isSenhaValid, isNotNull } from './../../shared/validations/validations';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { User } from '../models/user.model';
import { HttpResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';


@Component({
  templateUrl: './cadastrar.component.html',
  styleUrls: ['./cadastrar.component.css']
})
export class CadastrarComponent implements OnInit {

  public cadastrarForm: FormGroup;
  public processando: boolean;

  constructor(
    private _formBuilder: FormBuilder,
    private _authService: AuthService,
    private _toastrService: ToastrService,
    private _router: Router,
    private _validationService: ValidationsService
  ) { }

  ngOnInit(): void {
    this._criarCadastrarForm();
  }

  private _criarCadastrarForm(): void{
    this.cadastrarForm = this._formBuilder.group({
      nome: [null, [Validators.required, isNotNull]],
      email: [null, [Validators.required, Validators.email], [this._validationService.checarEmail()]],
      senha: [null, [Validators.required, Validators.minLength(6)]],
      reSenha: [null, [Validators.required, Validators.minLength(6)]],
    },{ validator: isSenhaValid});
  }

  public validarCampo(campo: string): boolean{
    return this.cadastrarForm.get(campo).invalid && this.cadastrarForm.get(campo).touched;
  }

  public validarSenha(): boolean{
    return this.cadastrarForm.hasError('senha') && this.cadastrarForm.get('senha').touched && this.cadastrarForm.get('reSenha').touched;
  }

  public cadastrar(): void{
    if(this.cadastrarForm.valid){
      this.processando = true;
      let user: User = User.converterFormToUser(this.cadastrarForm.value);
      this._authService.cadastrar(user).subscribe(
        (res: HttpResponse<User>) => {
          if(res.status == 201){
            if(res.body.id){
              this.processando = false;
              this._authService.salvarUsuario(res.body);
              this._toastrService.success(`Bem-vindo a plataforma.`);
              this._router.navigate(['/dashboard']);
            }else{
              this._toastrService.warning(`Ocorreu um erro ao tentar obter o cadastro do usuário, por favor tente novamente.`);
              this.processando = false;
            }
          }else{
            this._toastrService.warning(`Ocorreu um erro na resposta do servidor, por favor tente novamente.`);
            this.processando = false;
          }
        }, err => {
          this._toastrService.error(`Ocorreu um erro no processo de cadastro, por favor contate o suporte.`);
          this.processando = false;
        })
    }
  }

}
