import { LoginModel } from './models/login.model';
import { environment } from './../../environments/environment';
import { User } from './models/user.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http'
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _key: string = 'user';

  constructor(
    private _http: HttpClient
  ) { }

  public cadastrar(user: User): Observable<HttpResponse<User>>{
    return this._http.post<User>(`${environment.BASE_URL}/users`, user, {observe: "response"}).pipe(take(1));
  }

  /**
   * Executa o login.
   * @param login
   */
  efetuarLogin(login: LoginModel):Observable<HttpResponse<User[]>>{
    return this._http.get<User[]>(`${environment.BASE_URL}/users?email=${login.email}&senha=${login.senha}`, {observe: "response"}).pipe(take(1));
  }

  public salvarUsuario(user: User): void{
    localStorage.setItem(this._key, JSON.stringify(user));
  }

  public obterUsuarioLogado(): User{
    const user: User = JSON.parse(localStorage.getItem(this._key));
    if(user){
      return user;
    }
    return null;
  }

  /**
   * Verifica se o email já possui cadastro.
   * @param email
   */
  public verificarEmail(email: string): Observable<HttpResponse<User[]>>{
    return this._http.get<User[]>(`${environment.BASE_URL}/users?email=${email}`, {observe: "response"}).pipe(take(1));
  }

  /**
   * Verifica se a senha pertence ao usuario logado.
   * @param senha
   */
  public verificarSenha(senha: string): Observable<HttpResponse<User[]>>{
    let user = this.obterUsuarioLogado();
    return this._http.get<User[]>(`${environment.BASE_URL}/users?id=${user.id}&senha=${senha}`, {observe: "response"}).pipe(take(1));
  }

}
