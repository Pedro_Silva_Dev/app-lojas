import { User } from './../models/user.model';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from './../auth.service';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})

export class AuthComponent implements OnInit {

  @ViewChild('snack') _snack;

  loginForm: FormGroup;
  durationSnackBar: number = 3;
  processando: boolean;
  exibirSenha: boolean = false;

  constructor(
    private _formBuilder: FormBuilder,
    private _authService: AuthService,
    private _snackBar: MatSnackBar,
    private _toastrService: ToastrService,
    private _router: Router
  ) { }

  ngOnInit(): void {
    this._criarLoginForm();
  }

  private _criarLoginForm(): void{
    this.loginForm = this._formBuilder.group({
      email: [null, [Validators.required]],
      senha: [null, [Validators.required]]
    })
  };

  private _exibirMsgLogin(): void {
    this._snackBar.openFromComponent(this._snack, {
      duration: this.durationSnackBar * 1000,
      verticalPosition: 'top',
      panelClass: 'bg-success'
    });
  };

  public entrar(): void{
    if(this.loginForm.valid){
      this.processando = true;
      this._authService.efetuarLogin(this.loginForm.value).subscribe(
        (res: HttpResponse<User[]>) => {
          if(res.status == 200){
            let user: User = res.body.find(r => r.email == this.loginForm.value.email);
            if(user){
              this.processando = false;
              this._authService.salvarUsuario(user);
              this._exibirMsgLogin();
              this._router.navigate(['/dashboard']);
            }else{
              this._toastrService.warning(`Usuário não encontrado.`);
              this.processando = false;
            }
          }else{
            this._toastrService.warning(`Email ou senha inválidos`);
            this.processando = false;
          }
        }, err => {
          this._toastrService.error(`Ocorreu um erro ao tentar obter os dados do usuário, por favor entre em contato com o suporte.`);
          this.processando = false;
        });
    }
  };




}
