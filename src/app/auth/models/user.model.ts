export class User{
  id: number;
  nome: string;
  email: string;
  senha: string;

  constructor(id: number, nome: string, email: string, senha: string){
    this.id = id;
    this.nome = nome;
    this.email = email;
    this.senha = senha;
  }

  public static converterFormToUser(user: any): User{
    return new User(user.id, user.nome, user.email, user.senha);
  }

}
